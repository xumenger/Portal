>研究luna 的源码，大概摸清楚架构逻辑

本次纯粹是没有目的的小插曲，去看一下luna 的源码，以luna 程序编译运行一个.lua 文件为例梳理从main 开始的全流程，对于luna 的类设计、接口设计有一个大致的了解

之前一直在看lua 的源码，但是一直没有一个顶层的认知，这次就先切换到luna 再去换个角度和方法看一下

也思考一下怎么嵌入到Portal 中。luna 与lua 如何建立起联系。另外编译原理背后的计算机理论、数学知识也试着串一下！

先从Luna.cpp 里面的main() 开始

```c++
int main(int argc, const char **argv)
{
    luna::State state;

    lib::base::RegisterLibBase(&state);
    lib::io::RegisterLibIO(&state);
    lib::math::RegisterLibMath(&state);
    lib::string::RegisterLibString(&state);
    lib::table::RegisterLibTable(&state);

    if (argc < 2)
    {
        Repl(state);
    }
    else
    {
        ExecuteFile(argv, state);
    }

    return 0;
}
```

执行ExecuteFile() 方法

```c++
void ExecuteFile(const char **argv, luna::State &state)
{
    try
    {
        state.DoModule(argv[1]);
    }
    catch (const luna::OpenFileFail &exp)
    {
        printf("%s: can not open file %s\n", argv[0], exp.What().c_str());
    }
    catch (const luna::Exception &exp)
    {
        printf("%s\n", exp.What().c_str());
    }
}
```

可以看到核心还是`luna::State` 这个类型，那么就尝试去看一下`DoModule()` 方法

```c++
    void State::DoModule(const std::string &module_name)
    {
        LoadModule(module_name);
        if (CallFunction(stack_.top_ - 1, 0, 0))
        {
            VM vm(this);
            vm.Execute();
        }
    }

    void State::LoadModule(const std::string &module_name)
    {
        auto value = module_manager_->GetModuleClosure(module_name);
        if (value.IsNil())
            module_manager_->LoadModule(module_name);
        else
            *stack_.top_++ = value;
    }
```

先是`LoadModule()` 方法，其依赖`module_manager_`；然后是CallFunction()、VM

## 先切入到LoadModule()、ModuleManager看

`module_manager_` 在State.h 中的定义如下：

```c++
// Manage all modules
std::unique_ptr<ModuleManager> module_manager_;
```

ModuleManager.cpp 中其相关的方法如下，可以看到主要的调用步骤是：使用Lexer（词法分析器）解析为AST，进行语法分析，生成代码

```c++
    Value ModuleManager::GetModuleClosure(const std::string &module_name) const
    {
        Value key(state_->GetString(module_name));
        return modules_->GetValue(key);
    }

    void ModuleManager::LoadModule(const std::string &module_name)
    {
        io::text::InStream is(module_name);
        if (!is.IsOpen())
            throw OpenFileFail(module_name);

        Lexer lexer(state_, state_->GetString(module_name),
                    [&is] () { return is.GetChar(); });
        Load(lexer);

        // Add to modules' table
        Value key(state_->GetString(module_name));
        Value value = *(state_->stack_.top_ - 1);
        modules_->SetValue(key, value);
    }

    void ModuleManager::Load(Lexer &lexer)
    {
        // Parse to AST
        auto ast = Parse(&lexer);

        // Semantic analysis
        SemanticAnalysis(ast.get(), state_);

        // Generate code
        CodeGenerate(ast.get(), state_);
    }
```

对于语法树解析、语法分析、代码生成先暂时不看源码细节，回到`State.DoModule()` 这一层，去看`CallFunction()` 和`VM.Execute()`

## State.CallFunction() 方法解析

```c++
    bool State::CallFunction(Value *f, int arg_count, int expect_result)
    {
        assert(f->type_ == ValueT_Closure || f->type_ == ValueT_CFunction);

        // Set stack top when arg_count is fixed
        if (arg_count != EXP_VALUE_COUNT_ANY)
            stack_.top_ = f + 1 + arg_count;

        if (f->type_ == ValueT_Closure)
        {
            // We need enter next ExecuteFrame
            CallClosure(f, expect_result);
            return true;
        }
        else
        {
            CallCFunction(f, expect_result);
            return false;
        }
    }
```

没看懂

## 继续看VM 的代码

按照上面的分析，应该是ModuleManager 完成解析为AST、语法分析、生成代码，然后通过VM 来执行代码

在VM 中，核心的代码其实是`ExecuteFrame()`，如下所示，去逐个获取指令，然后按照指令的类型执行相应的逻辑

>此处暂停，因为如果深究需要挖掘的内容有很多，比如Luna 的基于栈的指令集设计就有很多值得挖掘的

```c++
    void VM::Execute()
    {
        assert(!state_->calls_.empty());

        while (!state_->calls_.empty())
        {
            // If current stack frame is a frame of a c function,
            // do not continue execute instructions, just return
            if (state_->calls_.back().func_->type_ == ValueT_CFunction)
                return ;
            ExecuteFrame();
        }
    }

    void VM::ExecuteFrame()
    {
        CallInfo *call = &state_->calls_.back();
        Closure *cl = call->func_->closure_;
        Function *proto = cl->GetPrototype();
        Value *a = nullptr;
        Value *b = nullptr;
        Value *c = nullptr;

        while (call->instruction_ < call->end_)
        {
            state_->CheckRunGC();
            Instruction i = *call->instruction_++;

            switch (Instruction::GetOpCode(i)) {
                case OpType_LoadNil:
                    a = GET_REGISTER_A(i);
                    GET_REAL_VALUE(a)->SetNil();
                    break;
                case OpType_FillNil:
                    a = GET_REGISTER_A(i);
                    b = GET_REGISTER_B(i);
                    while (a < b)
                    {
                        a->SetNil();
                        ++a;
                    }
                    break;
                case OpType_LoadBool:
                    a = GET_REGISTER_A(i);
                    GET_REAL_VALUE(a)->SetBool(Instruction::GetParamB(i) ? true : false);
                    break;
                case OpType_LoadInt:
                    a = GET_REGISTER_A(i);
                    assert(call->instruction_ < call->end_);
                    a->num_ = (*call->instruction_++).opcode_;
                    a->type_ = ValueT_Number;
                    break;
                case OpType_LoadConst:
                    a = GET_REGISTER_A(i);
                    b = GET_CONST_VALUE(i);
                    *GET_REAL_VALUE(a) = *b;
                    break;
                case OpType_Move:
                    a = GET_REGISTER_A(i);
                    b = GET_REGISTER_B(i);
                    *GET_REAL_VALUE(a) = *GET_REAL_VALUE(b);
                    break;
                case OpType_Call:
                    a = GET_REGISTER_A(i);
                    if (Call(a, i)) return ;
                    break;
                case OpType_GetUpvalue:
                    a = GET_REGISTER_A(i);
                    b = GET_UPVALUE_B(i)->GetValue();
                    *GET_REAL_VALUE(a) = *b;
                    break;
                case OpType_SetUpvalue:
                    a = GET_REGISTER_A(i);
                    b = GET_UPVALUE_B(i)->GetValue();
                    *b = *a;
                    break;
                case OpType_GetGlobal:
                    a = GET_REGISTER_A(i);
                    b = GET_CONST_VALUE(i);
                    *GET_REAL_VALUE(a) = state_->global_.table_->GetValue(*b);
                    break;
                case OpType_SetGlobal:
                    a = GET_REGISTER_A(i);
                    b = GET_CONST_VALUE(i);
                    state_->global_.table_->SetValue(*b, *a);
                    break;
                case OpType_Closure:
                    a = GET_REGISTER_A(i);
                    GenerateClosure(a, i);
                    break;
                case OpType_VarArg:
                    a = GET_REGISTER_A(i);
                    CopyVarArg(a, i);
                    break;
                case OpType_Ret:
                    a = GET_REGISTER_A(i);
                    return Return(a, i);
                case OpType_JmpFalse:
                    a = GET_REGISTER_A(i);
                    if (GET_REAL_VALUE(a)->IsFalse())
                        call->instruction_ += -1 + Instruction::GetParamsBx(i);
                    break;
                case OpType_JmpTrue:
                    a = GET_REGISTER_A(i);
                    if (!GET_REAL_VALUE(a)->IsFalse())
                        call->instruction_ += -1 + Instruction::GetParamsBx(i);
                    break;
                case OpType_JmpNil:
                    a = GET_REGISTER_A(i);
                    if (a->type_ == ValueT_Nil)
                        call->instruction_ += -1 + Instruction::GetParamsBx(i);
                    break;
                case OpType_Jmp:
                    call->instruction_ += -1 + Instruction::GetParamsBx(i);
                    break;
                case OpType_Neg:
                    a = GET_REGISTER_A(i);
                    CheckType(a, ValueT_Number, "neg");
                    a->num_ = -a->num_;
                    break;
                case OpType_Not:
                    a = GET_REGISTER_A(i);
                    a->SetBool(a->IsFalse() ? true : false);
                    break;
                case OpType_Len:
                    a = GET_REGISTER_A(i);
                    if (a->type_ == ValueT_Table)
                        a->num_ = a->table_->ArraySize();
                    else if (a->type_ == ValueT_String)
                        a->num_ = a->str_->GetLength();
                    else
                        ReportTypeError(a, "length of");
                    a->type_ = ValueT_Number;
                    break;
                case OpType_Add:
                    GET_REGISTER_ABC(i);
                    CheckArithType(b, c, "add");
                    a->num_ = b->num_ + c->num_;
                    a->type_ = ValueT_Number;
                    break;
                case OpType_Sub:
                    GET_REGISTER_ABC(i);
                    CheckArithType(b, c, "sub");
                    a->num_ = b->num_ - c->num_;
                    a->type_ = ValueT_Number;
                    break;
                case OpType_Mul:
                    GET_REGISTER_ABC(i);
                    CheckArithType(b, c, "multiply");
                    a->num_ = b->num_ * c->num_;
                    a->type_ = ValueT_Number;
                    break;
                case OpType_Div:
                    GET_REGISTER_ABC(i);
                    CheckArithType(b, c, "div");
                    a->num_ = b->num_ / c->num_;
                    a->type_ = ValueT_Number;
                    break;
                case OpType_Pow:
                    GET_REGISTER_ABC(i);
                    CheckArithType(b, c, "power");
                    a->num_ = pow(b->num_, c->num_);
                    a->type_ = ValueT_Number;
                    break;
                case OpType_Mod:
                    GET_REGISTER_ABC(i);
                    CheckArithType(b, c, "mod");
                    a->num_ = fmod(b->num_, c->num_);
                    a->type_ = ValueT_Number;
                    break;
                case OpType_Concat:
                    GET_REGISTER_ABC(i);
                    Concat(a, b, c);
                    break;
                case OpType_Less:
                    GET_REGISTER_ABC(i);
                    CheckInequalityType(b, c, "compare(<)");
                    if (b->type_ == ValueT_Number)
                        a->SetBool(b->num_ < c->num_);
                    else
                        a->SetBool(*b->str_ < *c->str_);
                    break;
                case OpType_Greater:
                    GET_REGISTER_ABC(i);
                    CheckInequalityType(b, c, "compare(>)");
                    if (b->type_ == ValueT_Number)
                        a->SetBool(b->num_ > c->num_);
                    else
                        a->SetBool(*b->str_ > *c->str_);
                    break;
                case OpType_Equal:
                    GET_REGISTER_ABC(i);
                    a->SetBool(*b == *c);
                    break;
                case OpType_UnEqual:
                    GET_REGISTER_ABC(i);
                    a->SetBool(*b != *c);
                    break;
                case OpType_LessEqual:
                    GET_REGISTER_ABC(i);
                    CheckInequalityType(b, c, "compare(<=)");
                    if (b->type_ == ValueT_Number)
                        a->SetBool(b->num_ <= c->num_);
                    else
                        a->SetBool(*b->str_ <= *c->str_);
                    break;
                case OpType_GreaterEqual:
                    GET_REGISTER_ABC(i);
                    CheckInequalityType(b, c, "compare(>=)");
                    if (b->type_ == ValueT_Number)
                        a->SetBool(b->num_ >= c->num_);
                    else
                        a->SetBool(*b->str_ >= *c->str_);
                    break;
                case OpType_NewTable:
                    a = GET_REGISTER_A(i);
                    a->table_ = state_->NewTable();
                    a->type_ = ValueT_Table;
                    break;
                case OpType_SetTable:
                    GET_REGISTER_ABC(i);
                    CheckTableType(a, b, "set", "to");
                    if (a->type_ == ValueT_Table)
                        a->table_->SetValue(*b, *c);
                    else if (a->type_ == ValueT_UserData)
                        a->user_data_->GetMetatable()->SetValue(*b, *c);
                    else
                        assert(0);
                    break;
                case OpType_GetTable:
                    GET_REGISTER_ABC(i);
                    CheckTableType(a, b, "get", "from");
                    if (a->type_ == ValueT_Table)
                        *c = a->table_->GetValue(*b);
                    else if (a->type_ == ValueT_UserData)
                        *c = a->user_data_->GetMetatable()->GetValue(*b);
                    else
                        assert(0);
                    break;
                case OpType_ForInit:
                    GET_REGISTER_ABC(i);
                    ForInit(a, b, c);
                    break;
                case OpType_ForStep:
                    GET_REGISTER_ABC(i);
                    i = *call->instruction_++;
                    if ((c->num_ > 0.0 && a->num_ > b->num_) ||
                        (c->num_ <= 0.0 && a->num_ < b->num_))
                        call->instruction_ += -1 + Instruction::GetParamsBx(i);
                    break;
                default:
                    break;
            }
        }

        Value *new_top = call->func_;
        // Reset top value
        state_->stack_.SetNewTop(new_top);
        // Set expect results
        if (call->expect_result_ != EXP_VALUE_COUNT_ANY)
            state_->stack_.SetNewTop(new_top + call->expect_result_);

        // Pop current CallInfo, and return to last CallInfo
        state_->calls_.pop_back();
    }
```

`CallInfo *call = &state_->calls_.back();` 显示，指令集是从State 里面拿到的，对应在State.h 中的定义如下

```c++
// Stack frames
std::list<CallInfo> calls_;
```

合理的分析就是在ModuleManager 进行AST 生成、语法分析、指令生成一系列步骤之后放到里面的。下面再贴一下`ModuleManager::Load(Lexer &lexer)` 的代码

```c++
    void ModuleManager::Load(Lexer &lexer)
    {
        // Parse to AST
        auto ast = Parse(&lexer);

        // Semantic analysis
        SemanticAnalysis(ast.get(), state_);

        // Generate code
        CodeGenerate(ast.get(), state_);
    }
```

## 词法分析器

在调用Parse() 之前，Lexer 是这么创建的

```c++
Lexer lexer(state_, state_->GetString(module_name),
                [&is] () { return is.GetChar(); });
```

简单看了一下Lexer.h 和Lexer.cpp 主要封装了如何按照lua 的语法来进行词法分析，解析得到token

## 解析AST

Parser.h 中定义了`Parse()` 方法

```c++
namespace luna
{
    class Lexer;
    class State;

    std::unique_ptr<SyntaxTree> Parse(Lexer *lexer);
} // namespace luna
```

Parser.cpp 里面是实现细节，代码很多，不贴到这里了！

核心就是按照Lua 的EBNF范式进行分析，构造抽象语法树

```
grammar Lua;

chunk
    : block EOF
    ;

block
    : stat* retstat?
    ;

stat
    : ';'
    | varlist '=' explist
    | functioncall
    | label
    | 'break'
    | 'goto' NAME
    | 'do' block 'end'
    | 'while' exp 'do' block 'end'
    | 'repeat' block 'until' exp
    | 'if' exp 'then' block ('elseif' exp 'then' block)* ('else' block)? 'end'
    | 'for' NAME '=' exp ',' exp (',' exp)? 'do' block 'end'
    | 'for' namelist 'in' explist 'do' block 'end'
    | 'function' funcname funcbody
    | 'local' 'function' NAME funcbody
    | 'local' attnamelist ('=' explist)?
    ;

attnamelist
    : NAME attrib (',' NAME attrib)*
    ;

attrib
    : ('<' NAME '>')?
    ;

retstat
    : 'return' explist? ';'?
    ;

label
    : '::' NAME '::'
    ;

funcname
    : NAME ('.' NAME)* (':' NAME)?
    ;

varlist
    : var (',' var)*
    ;

namelist
    : NAME (',' NAME)*
    ;

explist
    : exp (',' exp)*
    ;

exp
    : 'nil' | 'false' | 'true'
    | number
    | string
    | '...'
    | functiondef
    | prefixexp
    | tableconstructor
    | <assoc=right> exp operatorPower exp
    | operatorUnary exp
    | exp operatorMulDivMod exp
    | exp operatorAddSub exp
    | <assoc=right> exp operatorStrcat exp
    | exp operatorComparison exp
    | exp operatorAnd exp
    | exp operatorOr exp
    | exp operatorBitwise exp
    ;

prefixexp
    : varOrExp nameAndArgs*
    ;

functioncall
    : varOrExp nameAndArgs+
    ;

varOrExp
    : var | '(' exp ')'
    ;

var
    : (NAME | '(' exp ')' varSuffix) varSuffix*
    ;

varSuffix
    : nameAndArgs* ('[' exp ']' | '.' NAME)
    ;

nameAndArgs
    : (':' NAME)? args
    ;

/*
var
    : NAME | prefixexp '[' exp ']' | prefixexp '.' NAME
    ;
prefixexp
    : var | functioncall | '(' exp ')'
    ;
functioncall
    : prefixexp args | prefixexp ':' NAME args
    ;
*/

args
    : '(' explist? ')' | tableconstructor | string
    ;

functiondef
    : 'function' funcbody
    ;

funcbody
    : '(' parlist? ')' block 'end'
    ;

parlist
    : namelist (',' '...')? | '...'
    ;

tableconstructor
    : '{' fieldlist? '}'
    ;

fieldlist
    : field (fieldsep field)* fieldsep?
    ;

field
    : '[' exp ']' '=' exp | NAME '=' exp | exp
    ;

fieldsep
    : ',' | ';'
    ;

operatorOr
    : 'or';

operatorAnd
    : 'and';

operatorComparison
    : '<' | '>' | '<=' | '>=' | '~=' | '==';

operatorStrcat
    : '..';

operatorAddSub
    : '+' | '-';

operatorMulDivMod
    : '*' | '/' | '%' | '//';

operatorBitwise
    : '&' | '|' | '~' | '<<' | '>>';

operatorUnary
    : 'not' | '#' | '-' | '~';

operatorPower
    : '^';

number
    : INT | HEX | FLOAT | HEX_FLOAT
    ;

string
    : NORMALSTRING | CHARSTRING | LONGSTRING
    ;

// LEXER

NAME
    : [a-zA-Z_][a-zA-Z_0-9]*
    ;

NORMALSTRING
    : '"' ( EscapeSequence | ~('\\'|'"') )* '"'
    ;

CHARSTRING
    : '\'' ( EscapeSequence | ~('\''|'\\') )* '\''
    ;

LONGSTRING
    : '[' NESTED_STR ']'
    ;

fragment
NESTED_STR
    : '=' NESTED_STR '='
    | '[' .*? ']'
    ;

INT
    : Digit+
    ;

HEX
    : '0' [xX] HexDigit+
    ;

FLOAT
    : Digit+ '.' Digit* ExponentPart?
    | '.' Digit+ ExponentPart?
    | Digit+ ExponentPart
    ;

HEX_FLOAT
    : '0' [xX] HexDigit+ '.' HexDigit* HexExponentPart?
    | '0' [xX] '.' HexDigit+ HexExponentPart?
    | '0' [xX] HexDigit+ HexExponentPart
    ;

fragment
ExponentPart
    : [eE] [+-]? Digit+
    ;

fragment
HexExponentPart
    : [pP] [+-]? Digit+
    ;

fragment
EscapeSequence
    : '\\' [abfnrtvz"'\\]
    | '\\' '\r'? '\n'
    | DecimalEscape
    | HexEscape
    | UtfEscape
    ;
fragment
DecimalEscape
    : '\\' Digit
    | '\\' Digit Digit
    | '\\' [0-2] Digit Digit
    ;
fragment
HexEscape
    : '\\' 'x' HexDigit HexDigit
    ;
fragment
UtfEscape
    : '\\' 'u{' HexDigit+ '}'
    ;
fragment
Digit
    : [0-9]
    ;
fragment
HexDigit
    : [0-9a-fA-F]
    ;
COMMENT
    : '--[' NESTED_STR ']' -> channel(HIDDEN)
    ;
LINE_COMMENT
    : '--'
    (                                               // --
    | '[' '='*                                      // --[==
    | '[' '='* ~('='|'['|'\r'|'\n') ~('\r'|'\n')*   // --[==AA
    | ~('['|'\r'|'\n') ~('\r'|'\n')*                // --AAA
    ) ('\r\n'|'\r'|'\n'|EOF)
    -> channel(HIDDEN)
    ;
WS
    : [ \t\u000C\r\n]+ -> skip
    ;
SHEBANG
    : '#' '!' ~('\n'|'\r')* -> channel(HIDDEN)
    ;
```

## 语法分析

SemanticAnalysis.h 中定义了`SemanticAnalysis()` 方法

```c++
namespace luna
{
    class State;

    void SemanticAnalysis(SyntaxTree *root, State *state);
}
```

SemanticAnalysis.cpp 里面是实现细节，代码很多，不贴到这里了！这里面主要用到了观察者设计模式，大量的Visit() 方法

`void SemanticAnalysis(SyntaxTree *root, State *state)` 的入参为语法树SyntaxTree 对象、状态State 对象

```c++
    void SemanticAnalysis(SyntaxTree *root, State *state)
    {
        assert(root && state);
        SemanticAnalysisVisitor semantic_analysis(state);
        root->Accept(&semantic_analysis, nullptr);
    }
```

再去SyntaxTree.h、SyntaxTree.cpp 看SyntaxTree 的Accept() 方法

```c++
// SyntaxTree.h
#define SYNTAX_TREE_ACCEPT_VISITOR()                \
    virtual void Accept(Visitor *v, void *data) = 0
#define SYNTAX_TREE_ACCEPT_VISITOR_DECL()           \
    virtual void Accept(Visitor *v, void *data)


    class SyntaxTree
    {
    public:
        virtual ~SyntaxTree() { }
        SYNTAX_TREE_ACCEPT_VISITOR();
    };


// SyntaxTree.cpp
#define SYNTAX_TREE_ACCEPT_VISITOR_IMPL(class_name)         \
    void class_name::Accept(Visitor *v, void *data)         \
    {                                                       \
        v->Visit(this, data);                               \
    }

    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(Chunk)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(Block)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(ReturnStatement)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(BreakStatement)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(DoStatement)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(WhileStatement)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(RepeatStatement)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(IfStatement)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(ElseIfStatement)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(ElseStatement)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(NumericForStatement)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(GenericForStatement)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(FunctionStatement)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(FunctionName)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(LocalFunctionStatement)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(LocalNameListStatement)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(AssignmentStatement)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(VarList)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(Terminator)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(BinaryExpression)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(UnaryExpression)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(FunctionBody)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(ParamList)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(NameList)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(TableDefine)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(TableIndexField)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(TableNameField)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(TableArrayField)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(IndexAccessor)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(MemberAccessor)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(NormalFuncCall)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(MemberFuncCall)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(FuncCallArgs)
    SYNTAX_TREE_ACCEPT_VISITOR_IMPL(ExpressionList)
```

Chunk、Block、ReturnStatement 等都是SyntaxTree 的子类

其实就是调用了SemanticAnalysisVisitor 的Visit 方法

```c++
    class SemanticAnalysisVisitor : public Visitor
    {
    public:
        virtual void Visit(Chunk *, void *);
        virtual void Visit(Block *, void *);
        virtual void Visit(ReturnStatement *, void *);
        virtual void Visit(BreakStatement *, void *);
        virtual void Visit(DoStatement *, void *);
        virtual void Visit(WhileStatement *, void *);
        virtual void Visit(RepeatStatement *, void *);
        ...

    }
```

## 生成指令

CodeGenerate.h 中定义了`CodeGenerate()` 方法

```c++
namespace luna
{
    class State;

    void CodeGenerate(SyntaxTree *root, State *state);
} // namespace luna
```

其实现如下

```c++
    void CodeGenerate(SyntaxTree *root, State *state)
    {
        assert(root && state);
        CodeGenerateVisitor code_generator(state);
        root->Accept(&code_generator, nullptr);
    }
```

与SematicAnalysis 类似，最终也都是调用到CodeGenerateVisitor 的各种Visit() 方法上

```c++
    class CodeGenerateVisitor : public Visitor
    {
    public:
        explicit CodeGenerateVisitor(State *state)
            : state_(state), current_function_(nullptr) { }

        ~CodeGenerateVisitor()
        {
            while (current_function_)
            {
                DeleteCurrentFunction();
            }
        }

        virtual void Visit(Chunk *, void *);
        virtual void Visit(Block *, void *);
        virtual void Visit(ReturnStatement *, void *);
        virtual void Visit(BreakStatement *, void *);
        virtual void Visit(DoStatement *, void *);
        virtual void Visit(WhileStatement *, void *);
        virtual void Visit(RepeatStatement *, void *);
        virtual void Visit(IfStatement *, void *);
        virtual void Visit(ElseIfStatement *, void *);
        virtual void Visit(ElseStatement *, void *);
        virtual void Visit(NumericForStatement *, void *);
        virtual void Visit(GenericForStatement *, void *);
        virtual void Visit(FunctionStatement *, void *);
        virtual void Visit(FunctionName *, void *);
        virtual void Visit(LocalFunctionStatement *, void *);
        virtual void Visit(LocalNameListStatement *, void *);
        virtual void Visit(AssignmentStatement *, void *);
        ...
    }
```

CodeGenerate.cpp 里面是实现细节，代码很多，不贴到这里了！

## 简单总结

初步看了一下luna 的代码，核心还是递归的调用，lua 里面是通过递归来实现解析AST、语法分析、代码生成的吗？

另外关于lua 的指令集设计部分，其实我一直没有理解的清楚；关于lua 的GC 机制本次其实也没有去涉及！

接下来还是需要结合luna、lua 的代码一起去看

不过可以先不用纠结这么多，可以先在Portal 中设计一下，怎么嵌入支持lua 脚本去编写业务逻辑，考虑lua 与C++ 之间如何实现互相调用，分别需要设计哪些接口以便于去实现业务逻辑！
