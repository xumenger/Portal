>etcd 源码解读 - 1 - raft 算法层

[官网：https://etcd.io/](https://etcd.io/)

[raft 工程化案例之 etcd 源码实现](https://ai2news.com/blog/2597029/)

![](./image/20240317/01.png)

## 一些准备工作

在开始之前，先提出几个具体的问题：

1. etcd 有哪些模块，各个模块的功能是什么？各个模块之间如何协作？
2. etcd 对于Raft 是怎么封装的？类图？接口设计？
3. etcd 的Raft 于网络层之间是如何配合协作的？
4. etcd 的实现用到了Golang 的channel、goroutine，在C++ 上如何对标？
5. etcd 在K8s 里面具体是怎么应用的？

>raft 包下面是etcd 中对于Raft 的核心算法层实现；raft/raftpb 是Raft 的核心数据结构

>contrib/raftexmaple 是基于etcd raft 核心的一个应用层示例

在etcd 的实现中，算法层只聚焦于Raft 共识机制部分的内容，其中涉及网络通信、预写日志持久化、数据状态机管理的工作均在应用层实现。应用层是Raft 节点中承上启下的主干模块，既扮演了与外部客户端通信的服务端角色，也是下层算法库的使用者，还要负责与各模块交互，串联Raft 节点运行的整体流程

算法层是一个独立的goroutine，应用层也是一个独立的goroutine，两者之间以相对独立的角色互相存在

etcd 在实现Raft 时,将内容依据职责边界拆分为应用层和算法层两个模块，在引用关系上，算法层是一个被应用层引用的静态库，但在节点启动时，最终应用层和算法层的内容会内聚于两个独立的goroutine 当中，依据几个channel（recievec、tickc、propc、readyc、advanc……）进行模块间的异步通信

大概的流程梳理总结如下，etcd 主要用goroutine、channel 进行通信：

1. 客户端发送请求给到应用层，应用层收到请求后进行包装，通过propc 投递
2. 算法层通过propc 接收到请求之后，进行处理，比如预写日志
3. 算法层处理完之后，需要把结果给到应用层，通过readyc，通过Ready 结构体
4. Ready 中包含信息：
	* 哪些预写日志需要持久化，应用层来进行持久化
	* 哪些日志已经被集群中的多数派认可了，应用层可以应用到状态机中
5. 算法层不具备通信能力，也通过readyc，告诉应用层具体给哪些节点发送什么消息
6. advanc 和readyc 是成对出现的，应用层和算法层是一来一回的关系
7. 应用层通过readyc 获取指令后处理，处理完成后通过advanc 通知算法层
8. 还有一个receivec，表示应用层的请求不是来自客户端，而是来自其他节点，比如广播心跳
9. 还有一个tickc，定时器，应用层通过tickc 将自己角色的定时任务告诉算法层去处理
	* 比如，Leader 需要通过定时器向其他节点告知现在Leader 状态良好，不需要发起新一轮选举
	* Follower、Candidate 也有自己的定时逻辑


## 算法层核心类

#### Entry

`Entry` 位于./raft/raftpb/raft.pb.go 中，对应一个预写日志，包含了普通类型（写请求）和配置变更两种类型。`Entry` 中包含了任期Term、索引Index、内容Data 三个字段，其中Term 和Index 共同构成了`Entry` 的全局唯一索引

```go
type EntryType int32  
  
  
const (  
    EntryNormal     EntryType = 0  
    // 配置变更类的日志  
    EntryConfChange EntryType = 1  
)  
  
  
type Entry struct {  
    Term             uint64    `protobuf:"varint,2,opt,name=Term" json:"Term"`  
    Index            uint64    `protobuf:"varint,3,opt,name=Index" json:"Index"`  
    Type             EntryType `protobuf:"varint,1,opt,name=Type,enum=raftpb.EntryType" json:"Type"`  
    Data             []byte    `protobuf:"bytes,4,opt,name=Data" json:"Data,omitempty"`  
}
````

#### Message

算法层本身也是一个状态机的模式，对于算法层的节点，收到的都是封装成`Message` 类型的消息，`Message` 对于节点状态机是一个通用的结构体，收到`Message` 之后，根据`Message` 的类型、内容，决定算法层的节点应该从什么状态切换到什么状态

```go
type Message struct {  
    // Type：消息类型，宏观上分为请求和响应两大类（结尾带 Resp）；根据流程主线又可以分为日志同步、leader 选举、心跳广播、读请求四类；
    Type             MessageType `protobuf:"varint,1,opt,name=type,enum=raftpb.MessageType" json:"type"`  

    // To/From：消息的发送方和接收方，都以节点 id 进行标识
    To               uint64      `protobuf:"varint,2,opt,name=to" json:"to"`  
    From             uint64      `protobuf:"varint,3,opt,name=from" json:"from"`  

    // Term：消息发送方当前的任期
    Term             uint64      `protobuf:"varint,4,opt,name=term" json:"term"`  

    // LogTerm、Index：待同步日志的上一笔日志的任期和索引. leader 同步日志时用到这两个字段；
    LogTerm          uint64      `protobuf:"varint,5,opt,name=logTerm" json:"logTerm"`  
    Index            uint64      `protobuf:"varint,6,opt,name=index" json:"index"`  

    // Entries：待完成同步的预写日志；
    Entries          []Entry     `protobuf:"bytes,7,rep,name=entries" json:"entries"`  

    // Commit：leader 已提交的日志索引；
    Commit           uint64      `protobuf:"varint,8,opt,name=commit" json:"commit"`  
    // ...  

    // Reject：标识响应结果为拒绝或赞同. 节点响应竞选投票或者响应日志同步时会使用到此字段；
    Reject           bool        `protobuf:"varint,10,opt,name=reject" json:"reject"`  

    // RejectHint：节点上一条预写日志的索引. 节点响应日志同步时使用到此字段.
    RejectHint       uint64      `protobuf:"varint,11,opt,name=rejectHint" json:"rejectHint"`  
    // ...  
}
```

`Message` 作为一个大而全的通用结构体，同时耦合了日志同步请求/响应、Leader 选举请求/响应、心跳请求/响应等内容，而在每次使用上根据当前流程往往只涉及到其中的少量字段。综合优劣，个人觉得是一种颇有争议的实现方式

>可以去对应看一下Kafka 的源码，在Kafka 中也是有针对不同的消息类型进行不同的逻辑处理的。其实这部分的逻辑适合使用策略模式进行封装

```proto
type MessageType int32  
  
const ( 
    // 本节点要进行选举  
    MsgHup            MessageType = 0  
    // MsgBeat不用于节点之间通信，仅用于leader内部HB时间到了让leader发送HB消息  
    MsgBeat           MessageType = 1  
    // 用户向raft提交数据  
    MsgProp           MessageType = 2  
    // leader向集群中其他节点同步数据  
    MsgApp            MessageType = 3  
    // append消息的应答  
    MsgAppResp        MessageType = 4  
    // 投票消息  
    MsgVote           MessageType = 5  
    MsgVoteResp       MessageType = 6  
    // ...  
    MsgHeartbeat      MessageType = 8  
    MsgHeartbeatResp  MessageType = 9  
    // ...  
    MsgReadIndex      MessageType = 15  
    MsgReadIndexResp  MessageType = 16  
    MsgPreVote        MessageType = 17  
    MsgPreVoteResp    MessageType = 18  
)
```

#### raftLog

raftLog 代码位于./raft/log.go 文件中；unstable 代码位于./raft/log_unstable.go 文件中；storage 代码位于./raft/storage.go 文件中

Raft 中对于日志的管理，比如下面的这个例子。commit index 左边的日志才是**被集群所认可**的；apply index 表示被应用到状态机的日志索引；commit index 右侧的日志后续可能会被回滚，并不稳定；stable index 左侧的日志表示的是已经被持久化的日志，已经**在这个节点上**落盘了，不会因为节点的宕机而丢失，stable index 右侧的日志对应就在unstable 中

真正把日志持久化，落到磁盘中的动作其实是在应用层实现的，具体在wal 模块，所以etcd 的实现分层、模块划分值得研究。算法层的raftLog 在一定程度上和应用层所处理的预写日志持久化的模块有一定的交互，但仅限于查询。算法层是不关注日志具体如何写到磁盘上的

算法层不具备把预写日志持久化到磁盘的这个能力，所以先把预写日志缓存在内存组织的unstable 中，当明确预写日志可以持久化到磁盘之后，则把Message 封装成Ready 的类型，通过readyc 给到应用层，由应用层来做持久化的动作。持久化之后，应用层通过anvancec 通道告诉算法层已经持久化成功，算法层就可以将其从unstable 中移除了

>至此，一直以来不是很理解的wal、unstable、storage 终于算是串起来了，etcd 的源码还是要持续研究。但是我在Portal 中需要做的这么复杂吗？

```
1  2  3  4  5  6  7  8  9  10  11  12  13  14  15  16  17    log index
   ^                        ^                  ^
   |                        |                  |
apply index           commit index        stable index
```

raftLog 是算法层中管理预写日志的模块，包含未持久化预写日志读写模块unstable 和持久化日志查询模块storage

```go
type raftLog struct {  
    // 用于保存自从最后一次snapshot之后提交的数据  
    storage Storage  

    // 用于保存还没有持久化的数据和快照，这些数据最终都会保存到storage中  
    unstable unstable  
  
    // 已提交日志的索引
    // committed数据索引  
    committed uint64  
  
    // 已应用日志的索引.
    // committed保存是写入持久化存储中的最高index，而applied保存的是传入状态机中的最高index  
    // 即一条日志首先要提交成功（即committed），才能被applied到状态机中  
    // 因此以下不等式一直成立：applied <= committed  
    applied uint64  
    // ...  
}
```

预写日志产生时，需要经历一个未持久化（内存）-> 已持久化（磁盘）的过程，前者在算法层内由raftLog.unstable 完成，后者在应用层内完成，并通过raftLog.storage 为算法层提供已持久化预写日志的查询能力

storage：持久化日志存储接口，提供了日志的查询能力。storage 是一个抽象接口，可由用户自定义实现，核心api 如下；

```go
type Storage interface {  
    // 返回保存的初始状态  
    InitialState() (pb.HardState, pb.ConfState, error)  
    // 返回索引范围在[lo,hi)之内并且不大于maxSize的entries数组  
    Entries(lo, hi, maxSize uint64) ([]pb.Entry, error)  
    // 传入一个索引值，返回这个索引值对应的任期号，如果不存在则error不为空，其中：  
    // ErrCompacted：表示传入的索引数据已经找不到，说明已经被压缩成快照数据了。  
    // ErrUnavailable：表示传入的索引值大于当前的最大索引  
    Term(i uint64) (uint64, error)  
    // 获得最后一条数据的索引值  
    LastIndex() (uint64, error)  
    // 返回第一条数据的索引值  
    FirstIndex() (uint64, error)  
    // ...  
}
```

unstable：提供了未持久化预写日志代理能力，可读可写；entries 是还未持久化的预写日志列表；offset 是首笔未持久化预写日志在全局预写日志中的索引偏移量

```go
type unstable struct {  
    // ...  
    // 还未持久化的数据  
    entries []pb.Entry  
    // offset用于保存entries数组中的数据的全局的起始index  
    offset  uint64  
    // ...  
}
```

#### Ready

Ready 是算法层与应用层交互的数据格式。每当算法层执行完一轮处理逻辑后，会往一个channel 中（readyc） 传入一个Ready 结构体，其中封装了算法层处理好的结果

```go
type Ready struct {  
    // 软状态是异变的，包括：当前集群leader、当前节点状态  
    *SoftState  
  
  
    // 硬状态需要被保存，包括：节点当前Term、Vote、Commit  
    // 如果当前这部分没有更新，则等于空状态  
    pb.HardState  
  
  
    // 需要在消息发送之前被写入到持久化存储中的entries数据数组  
    // 本轮算法层产生的预写日志，此时还未持久化，需要传输到应用层，由应用层完成持久化
    Entries []pb.Entry  
  
  
    // ...  
    // 需要输入到状态机中的数据数组，这些数据之前已经被保存到持久化存储中了  
    // 本轮算法层已提交的预写日志，需要传输到应用层，由应用层将其应用到状态机
    CommittedEntries []pb.Entry  
  
  
    // 在entries被写入持久化存储中以后，需要发送出去的数据
    // 本轮算法层需要发出的消息，由应用层调用网络通信模块发出
    Messages []pb.Message  
}
```

SoftState：raft 节点的软状态，包含了节点的Leader 和角色状态两部分信息，由于都是容易发生改变且通过通信可重新恢复的信息，因此无需进行持久化存储（不需要持久化，所以称为软状态，可能频繁的发生变化）

```go
type SoftState struct {  
    Lead      uint64 // must use atomic operations to access; keep 64-bit aligned.  
    RaftState StateType  
}


type StateType uint64  
  
  
const (  
    StateFollower StateType = iota  
    StateCandidate  
    StateLeader  
    StatePreCandidate  
)
```

HardState：raft 节点的当前任期、投票归属和已提交日志索引，这些信息都需要持久化存储，节点宕机后重启亦可恢复如初

```go
type HardState struct {  
    Term             uint64 `protobuf:"varint,1,opt,name=term" json:"term"`  
    Vote             uint64 `protobuf:"varint,2,opt,name=vote" json:"vote"`  
    Commit           uint64 `protobuf:"varint,3,opt,name=commit" json:"commit"`  
}
```

#### Node

这里涉及到应用层与算法层的架构了。应用层有一个类型叫做raftNode，会有一个算法层的引用，也就是Node，当应用层需要使用算法层什么能力的时候，就通过调用Node 的方法来使用算法层的能力。**调用Node 层的方法，其实就是channel 给算法层的goroutine 发送一些消息，让算法进行相应的一些处理，并不是同步调用的**

>在C++ 中没有channel、goroutine 的概念，需要考虑一下应用层与算法层之间的交互怎么通过同步调用的方式对etcd 的逻辑进行改造！

Node 层代表应用层看到的算法层的抽象

```go
// Node represents a node in a raft cluster.  
type Node interface {  
    // 定时驱动
    // 传送定时驱动信号，每次调用 Tick 方法的时间间隔是固定的，称为一个 tick
    // 是 raft 节点的最小计时单位
    // 后续 leader 节点的心跳计时和 leader/candidate 的选举计时也都是以 tick 作为时间单位
    Tick()  

    // 应用层调用 Node.Propose 方法，向算法层发起一笔写数据的请求
    Propose(ctx context.Context, data []byte) error  

    // 应用层调用该方法，通过 node.propc hannel 向算法层发送一则消息类型为 MsgProp、日志类型为 EntryConfChange 的消息
    // 推动 raft 节点进入配置变更流程
    ProposeConfChange(ctx context.Context, cc pb.ConfChange) error  

    // 应用层调用该方法的时机是在配置变更提议已经通过两阶段提交之后
    // 该方法会通过 node.confc 向算法层发送配置变更内容
    // 算法层 goroutine 接收到后会直接应用，使得变更即时生效
    ApplyConfChange(cc pb.ConfChange) *pb.ConfState  

    // 应用层调用 node.ReadIndex 方法向算法层发起读数据请求
    // 实际上会往 node.recvc channel 中传入一条类型为 MsgReadIndex 的消息
    ReadIndex(ctx context.Context, rctx []byte) error  

    // 应用层调用 Node.Step 方法，可以向算法层 goroutine 传送一条自由指定类型的消息
    // 但类型不能是 MsgHup（驱动本节点进行选举）或者 MsgBeat（驱动本 leader 节点进行心跳广播）
    // 因为这些动作应该是由定时器驱动，而非人为调用
    Step(ctx context.Context, msg pb.Message) error  


    Ready() <-chan Ready  
    Advance()  
    // ...  
}
```

应用层调用Node.Ready 方法返回node.readyc channel 用于监听，当应用层从readyc 中读取到Ready 信号时，说明算法层已经产生了新一轮的处理结果，应用层需要进行响应处理

当应用层处理完毕后，需要调用Node.Advance 方法，通过向node.advancec channel 中发送信号的方式，示意应用层已处理完毕，算法层可以进入下一轮调度循环

因此，Node.Ready 和Node.Advance 方法是成对使用的，当两个方法各被调用一次，意味着应用层与算法层之间完成了一轮交互，紧接着会开启下一轮，周而复始，循环往复

#### readOnly

代码位于 ./raft/read_only.go 文件。readOnly 是挂起的读请求队列，由raft 节点持有

```go
type readOnly struct {  
    // 使用entry的数据为key，保存当前pending的readIndex状态  
    // 是一系列还未处理的读请求，通过 map 的形式存储，建立读请求 id 与读请状态间的映射关系
    pendingReadIndex map[string]*readIndexStatus  

    // 保存entry的数据为的队列，pending的readindex状态在这个队列中进行排队  
    readIndexQueue   []string  
}
```

readIndexQueue：读请求id 数组，本质上是个队列，根据读请求到达的时间顺序先进先出

```go
type readIndexStatus struct {  
    // 保存原始的readIndex请求消息  
    req   pb.Message  

    // 保存收到该readIndex请求时的leader commit 索引  
    index uint64  

    // 保存有什么节点进行了应答，从这里可以计算出来是否有超过半数应答了  
    acks  map[uint64]struct{}  
}
```

acks 的理解：对leader 节点而言，响应读请求前需要证实自己身份的合法性，所以会给所有节点广播一轮特殊的心跳请求，并通过此处的acks map 记录有哪些节点进行了响应，当达到多数派，就可以对读请求作出响应

#### Progress

代码位于 ./raft/progress.go 文件

```go
type Progress struct {  
    Match, Next uint64  
}
```

Progress 是Leader 记录其他节点日志同步进度的载体，Match 是该节点已同步日志的索引；Next 是leader 下一次向节点同步日志时的日志索引

>Match 和Next 之间是不是还有in flight 的日志？是不是可以按照下面的例子理解？

>比如Leader 发生1、2、3、4 日志给到Follower 1，Follower 1 应答2 已经收到了，那么Match 为2，而3、4 属于in flight，但是其实3、4 已经发送了，所以接下来需要发生的是5，也就是Next 是5

Leader 通过检查所有Follower 的Match 就可以获知目前哪个index 已经被集群中的多数派认可了

#### raft

代码位于 ./raft/raft.go。Node 是应用层视角对于算法层的抽象，应用层需要通过Node 来和算法层进行交互。在算法层运行的真正过程中，在算法层的视角中，一个算法层的节点对应的是raft 类。raft 囊括了一个算法层节点需要健康运行所需要的全部信息

```go
type raft struct {  
    id uint64  

    // 任期号  
    Term uint64  

    // 投票给哪个节点ID  
    Vote uint64  

    raftLog *raftLog  
    prs         map[uint64]*Progress  
    state StateType  

    // 该map存放哪些节点投票给了本节点  
    votes map[uint64]bool  

    msgs []pb.Message  
    lead uint64  

    // 标识当前还有没有applied的配置数据  
    pendingConf bool  

    readOnly *readOnly  
    electionElapsed int  
    heartbeatElapsed int  
    preVote     bool  
    heartbeatTimeout int  
    electionTimeout  int  
    randomizedElectionTimeout int  

    // tick函数，在到期的时候调用，不同的角色该函数不同  
    tick func()  

    // 状态机函数
    // 节点的状态机处理函数，不同角色的状态机函数不同，分为 stepLeader、stepCandidate 和 stepFollower 三类
    step stepFunc  
}  
  
  
type stepFunc func(r *raft, m pb.Message)
```


## 应用层核心类

本节中，以etcd 提供的raft 运行示例作为参照，学习应用层的代码架构和实现细节。为了使得整个流程更加连贯和立体，此处还会简单谈及的kvStore（数据状态机）和httpapi（客户端）的一些内容

#### raftNode

代码位于 ./contrib/raftexample/raft.go

raftNode 是在应用层中额外封装了一层的raft 节点，处理持有算法层的入口之外，还包含了与客户端、数据状态机、预写日志持久化模块、通信模块交互的一些能力

```go
type raftNode struct {  
    // 用于接收客户端发送的写请
    proposeC    <-chan string            // proposed messages (k,v)  

    // 用于接收客户端发送的配置变更请求
    confChangeC <-chan raftpb.ConfChange // proposed cluster config changes  

    // 用于将已提交的日志应用到数据状态机
    commitC     chan<- *string           // entries committed to log (k,v)  

    // ...  

    // raft 节点 id
    id          int      // client ID for raft session  

    // 同一集群内其他 raft 节点的标识信息
    peers       []string // raft peer URLs  

    // ...  
  
    // 本节点已应用到状态机的日志索引
    appliedIndex  uint64  
  
  
    // raft backing for the commit/error channel  
    // 算法层入口
    node        raft.Node  

    // raftexample 的持久化预写日志存储模块，用内存实现
    // etcd 的真实应用层实现，对应在wal 里面
    raftStorage *raft.MemoryStorage  
  
  
    // ...  

    // raft 集群通信模块
    transport *rafthttp.Transport  
    // ...  
}
```

#### kvstore

代码位于 ./contrib/raftexample/kvstore.go。kvstore 是一个乞丐版的键值对存储模块，用于以小见大，模拟还原etcd 存储系统与raft 节点间的交互模式

```go
type kvstore struct {  
    // 与 raftNode.proposeC 是同一个 channel，负责向 raftNode 发送来自客户端的写数据请求
    proposeC    chan<- string // channel for proposing updates  

    // 读写锁，保证数据并发安全
    mu          sync.RWMutex  

    // 键值对存储介质，可以理解为，它就是数据状态机
    kvStore     map[string]string // current committed key-value pairs  
}
```

kvstore 模块启动时，会注入一个commitC channel，和raftNode.commitC 是同一个 channel

```go
func newKVStore(snapshotter *snap.Snapshotter, proposeC chan<- string, commitC <-chan *string, errorC <-chan error) *kvstore {  
    s := &kvstore{proposeC: proposeC, kvStore: make(map[string]string), snapshotter: snapshotter}  
     
    // read commits from raft into kvStore map until error  
    go s.readCommits(commitC, errorC)  
    return s  
}
```

kvstore 会持续监听该channel，获取到raftNode 提交的日志数据，将其应用到数据状态机

```go
func (s *kvstore) readCommits(commitC <-chan *string, errorC <-chan error) {  
    for data := range commitC {  
        // ...  
        var dataKv kv  
        dec := gob.NewDecoder(bytes.NewBufferString(*data))  
        if err := dec.Decode(&dataKv); err != nil {  
            log.Fatalf("raftexample: could not decode message (%v)", err)  
        }  
        s.mu.Lock()  
        s.kvStore[dataKv.Key] = dataKv.Val  
        s.mu.Unlock()  
    }  
    // ...  
}
```

此外，kvstore 提供了Propose 方法供客户端发送写请求，kvstore 随之通过proposeC 将请求发送给raftNode 处理

```go
func (s *kvstore) Propose(k string, v string) {  
    var buf bytes.Buffer  
    if err := gob.NewEncoder(&buf).Encode(kv{k, v}); err != nil {  
        log.Fatal(err)  
    }  
    s.proposeC <- string(buf.Bytes())  
}
```

#### httpapi

代码位于 ./contrib/raftexample/httpapi.go。是raftexample 中给出的一个HTTP Server 的示例

服务运行时，如果接收到PUT 请求，则视为一个写数据请求，会通过kvstore.Propose api 进行转发处理；如果是POST 请求，会视为添加节点的配置变更请求；如果是DELETE 请求，会视为删除节点的集群配置变更请求

```go
type httpKVAPI struct {  
    store       *kvstore  
    confChangeC chan<- raftpb.ConfChange  
}  
  
  
func (h *httpKVAPI) ServeHTTP(w http.ResponseWriter, r *http.Request) {  
    key := r.RequestURI  
    switch {  
    case r.Method == "PUT":  
        v, err := ioutil.ReadAll(r.Body)  
        // ...  
        h.store.Propose(key, string(v))  
    // ...  
    case r.Method == "POST":  
        url, err := ioutil.ReadAll(r.Body)  
        // ...  
  
  
        cc := raftpb.ConfChange{  
            Type:    raftpb.ConfChangeAddNode,  
            NodeID:  nodeId,  
            Context: url,  
        }  
        h.confChangeC <- cc  
  
  
    // ...  
    case r.Method == "DELETE":  
        // ...  
  
  
        cc := raftpb.ConfChange{  
            Type:   raftpb.ConfChangeRemoveNode,  
            NodeID: nodeId,  
        }  
        h.confChangeC <- cc  
    // ...  
}
```

## 应用层启动流程

应用层封装的raft 节点定义为raftNode 类，随着raftNode.startRaft 方法的调用，一个raft 节点真正启用了，其中包含了应用层和算法层两个部分的初始化和启动过程

代码位于 ./conrib/raft/example/raft.go

```go
func (rc *raftNode) startRaft() {  
    // ...  
  
  
    // 获取集群内其他raft 节点的信息，分配id
    rpeers := make([]raft.Peer, len(rc.peers))  
    for i := range rpeers {  
        rpeers[i] = raft.Peer{ID: uint64(i + 1)}  
    }  

    // 创建一份 raft 节点配置，其中 leader 的心跳时间间隔默认为 tick
    // follower/candidate 的选举时间间隔默认为 10 个 tick
    c := &raft.Config{  
        ID:              uint64(rc.id),  
        ElectionTick:    10,  
        HeartbeatTick:   1,  
        Storage:         rc.raftStorage,  
    }  
  
  
    startPeers := rpeers  

    // 启动算法层的一个Node，此处这是算法层与应用层的分水岭
    rc.node = raft.StartNode(c, startPeers)  
  
  
    // ...  
  
    // 启动通信模块
    rc.transport = &rafthttp.Transport{  
        ID:          types.ID(rc.id),  
        ClusterID:   0x1000,  
        Raft:        rc,  
        ServerStats: ss,  
        LeaderStats: stats.NewLeaderStats(strconv.Itoa(rc.id)),  
        ErrorC:      make(chan error),  
    }  
  
    rc.transport.Start()  
    for i := range rc.peers {  
        if i+1 != rc.id {  
            rc.transport.AddPeer(types.ID(i+1), []string{rc.peers[i]})  
        }  
    } 
  
    go rc.serveRaft()  

    // 异步开启 raftNode 的主循环
    // 用于与算法层的 goroutine 建立持续通信的关系
    go rc.serveChannel  
}
```

下面展开raftNode.serveChannels 方法的详细内容

1. 应用层发送请求，通过propc、recvc、conf 给到算法层
2. 算法层响应处理结果，通过readyc 响应给应用层
3. 应用层示意处理完毕，通过advancec 告知算法层

```go
func (rc *raftNode) serveChannels() {  
    // ...  
  
  
    ticker := time.NewTicker(100 * time.Millisecond)  
    defer ticker.Stop()  
  
  
    go func() {  
        var confChangeCount uint64 = 0  
  
        // Propose & ProposeConfChange
        // raftNode 会持续监听 proposeC 和 confChangeC 两个 channel
        // 从而接收到来自客户端的写数据和配置变更请求
        // 然后调用 Node 接口的 api 将其发送给算法层
        for rc.proposeC != nil && rc.confChangeC != nil {  
            select {  
            case prop, ok := <-rc.proposeC:  
                if !ok {  
                    rc.proposeC = nil  
                } else {  
                    // blocks until accepted by raft state machine  
                    rc.node.Propose(context.TODO(), []byte(prop))  
                }  
  
            case cc, ok := <-rc.confChangeC:  
                if !ok {  
                    rc.confChangeC = nil  
                } else {  
                    confChangeCount += 1  
                    cc.ID = confChangeCount  
                    rc.node.ProposeConfChange(context.TODO(), cc)  
                }  
            }  
        }  
        // client closed channel; shutdown raft if not already  
        close(rc.stopc)  
    }()  
  
  
    // Tick & Ready & Advance
    // event loop on raft state machine updates  
    for {  
        select {  
        case <-ticker.C:  
            rc.node.Tick()  
  
  
        // store raft entries to wal, then publish over commit channel  
        case rd := <-rc.node.Ready():  
            // ...  
            rc.raftStorage.Append(rd.Entries)  
            rc.transport.Send(rd.Messages)  
            if ok := rc.publishEntries(rc.entriesToApply(rd.CommittedEntries)); !ok {  
                rc.stop()  
                return  
            }  
            rc.node.Advance()  
  
  
        // ...  
        }  
}
```


## 算法层启动流程

算法层raft 节点的启动过程。代码内容位于./raft/node.go

```go
func StartNode(c *Config, peers []Peer) Node {  
    // 启动 node 时，先要初始化一个 raft 共识机制的抽象结构
    r := newRaft(c)  

    // 节点刚启动时，都统一置为 follower，把任期设置为 1
    // （1 是最小的任期，哪怕任期暂时滞后，也可以随着通信逐渐恢复）
    // 把 leader 置为 None
    r.becomeFollower(1, None)  

    // 把集群中其他节点都封装成添加节点的配置变更信息，添加到非持久化预写日志中
    for _, peer := range peers {  
        cc := pb.ConfChange{Type: pb.ConfChangeAddNode, NodeID: peer.ID, Context: peer.Context}  
        d, err := cc.Marshal()  
        if err != nil {  
            panic("unexpected marshal error")  
        }  
        e := pb.Entry{Type: pb.EntryConfChange, Term: 1, Index: r.raftLog.lastIndex() + 1, Data: d}  
        r.raftLog.append(e)  
    }  
      
    // 启动之初的配置变更日志直接视为已提交
    r.raftLog.committed = r.raftLog.lastIndex()  
      
    // 将集群中其他节点的日志同步进度添加到进度 map prs 当中
    for _, peer := range peers {  
        r.addNode(peer.ID)  
    }  
  
  
    // 初始化节点
    n := newNode()  

    // 异步调用node.run 方法，启动算法层raft 节点goroutine
    // 未来正是这个goroutine 持续与应用层进行通信交互
    go n.run(r)  

    return &n  
}
```

node.run 方法，启动算法层raft 节点goroutine，未来正是这个goroutine 持续与应用层进行通信交互

```go
func (n *node) run(r *raft) {  
    var propc chan pb.Message  
    var readyc chan Ready  
    var advancec chan struct{}  
    var prevLastUnstablei, prevLastUnstablet uint64  
    var havePrevLastUnstablei bool  
    var rd Ready  
  
  
    lead := None  
    prevSoftSt := r.softState()  
    prevHardSt := emptyState  
  
  
    // 通过 channel 指针替换，保证在 select 多路复用的模式下
    // Node.Ready（readyc）和Node.Advance（advancec）方法是被成对调用的
    for {  
        if advancec != nil {  
            // advance channel不为空，说明还在等应用调用Advance接口通知已经处理完毕了本次的ready数据  
            readyc = nil  
        } else {  
        	// 保证算法层没有新结果产生时，不会通过 readyc 向应用层提交 Ready 消息，避免流程空转
            rd = newReady(r, prevSoftSt, prevHardSt)  
            if rd.containsUpdates() {  
                // 如果这次ready消息有包含更新，那么ready channel就不为空  
                readyc = n.readyc  
            } else {  
                // 否则为空  
                readyc = nil  
            }  
        }  
          
        // 接收到来自应用层的消息时，会步入 raft.step 方法中
        // ...  
        select {  
        case m := <-propc:  
            // 处理本地收到的提交值  
            m.From = r.id  
            r.Step(m)  
        case m := <-n.recvc:  
            // 处理其他节点发送过来的提交值  
            // filter out response message from unknown From.  
            if _, ok := r.prs[m.From]; ok || !IsResponseMsg(m.Type) {  
                // 需要确保节点在集群中或者不是应答类消息的情况下才进行处理  
                r.Step(m) // raft never returns an error  
            }  
        // 接收到已经确认可应用的配置变更数据时，会对集群配置发起变更
        case cc := <-n.confc:  
            // 接收到配置发生变化的消息  
            if cc.NodeID == None {  
                // NodeId为空的情况，只需要直接返回当前的nodes就好  
                r.resetPendingConf()  
                select {  
                case n.confstatec <- pb.ConfState{Nodes: r.nodes()}:  
                case <-n.done:  
                }  
                break  
            }  
            switch cc.Type {  
            case pb.ConfChangeAddNode:  
                r.addNode(cc.NodeID)  
            case pb.ConfChangeRemoveNode:  
                // 如果删除的是本节点，停止提交  
                if cc.NodeID == r.id {  
                    propc = nil  
                }  
                r.removeNode(cc.NodeID)  
            // ...  
            }  
            // ...  
        // 接收到应用层的定时Tick 调用时，会根据raft 节点的角色，使用其对应的tick 函数进行处理
        case <-n.tickc:  
            r.tick()  

        // 算法层有处理结果后会投递到readyc 当中，供应用层raftNode 接收，并更新一些状态变量
        // 应用层处理完成后会往advancec 中发送信号量，算法层会更新applied index，并开启新一轮循环
        case readyc <- rd:  
            // 通过channel写入ready数据  
            // 以下先把ready的值保存下来，等待下一次循环使用，或者当advance调用完毕之后用于修改raftLog的  
            if rd.SoftState != nil {  
                prevSoftSt = rd.SoftState  
            }  
            if len(rd.Entries) > 0 {  
                // 保存上一次还未持久化的entries的index、term  
                prevLastUnstablei = rd.Entries[len(rd.Entries)-1].Index  
                prevLastUnstablet = rd.Entries[len(rd.Entries)-1].Term  
                havePrevLastUnstablei = true  
            }  
            if !IsEmptyHardState(rd.HardState) {  
                prevHardSt = rd.HardState  
            }  
            // ...  
            r.msgs = nil  
            r.readStates = nil  
            // 修改advance channel不为空，等待接收advance消息  
            advancec = n.advancec  
        case <-advancec:  
            // 收到advance channel的消息  
            if prevHardSt.Commit != 0 {  
                // 将committed的消息applied  
                r.raftLog.appliedTo(prevHardSt.Commit)  
            }  
            advancec = nil  
            // ...  
    }  
}
```


## 角色转换

TODO


## 写请求处理流程

TODO


## 读请求处理流程

TODO
