>etcd 源码解读 - 2 - 应用层

本文主要站在宏观层面去介绍etcd，对于etcd 内部实现的时候，应用层与Raft 层之间通过什么channel 传递什么信息等实现细节是没有涉及到介绍的！


## 读请求流程

etcd 是典型的读多写少存储，在我们实际业务场景中，读一半占据2/3 以上的请求，一个读请求从client 通过Round-Robin 负载均衡算法，选择一个etcd server 节点，发出gRPC 请求，经过etcd server 的KVServer 模块、线性读模块、MVCC 的treeIndex 和BoltDB 模块紧密协作，完成一个读请求

下面以一个例子来将读请求的完整流程，比如客户端发起请求

```shell
etcdctl get hello --endpoints 192.168.65.210:2379, 192.168.65.211:2379, 192.168.65.212:2379
```

#### 客户端

首先，etcdctl 会对命令中的参数进行解析。get 是请求的方法，它是KVServer 模块的API；hello 是需要查询的key 名；endpoints 是后端的etcd 集群地址，通常生产上需要配置多个，这样在某个节点故障之后，client 就可以自动重连到其他正常的节点上，从而保证请求的正常执行

解析完请求中的参数之后，etcdctl 会创建一个clientv3 库对象，使用KVServer 模块的API 来访问etcd server。

client 发送Range RPC 请求到server 后就进入了KVServer 模块。etcd 通过拦截器以非侵入的方式实现许多特性，比如丰富的metrics、日志、请求行为检查、所有请求的耗时等

#### 串行读和线性读

etcd 为了保证服务高可用，生产环境一般部署多个节点，多节点之间的数据由于延迟等关系可能存在不一致的情况

当client 发起一个写请求之后会分为以下步骤：

1. Leader 收到写请求，它会将此请求持久化到WAL 日志，并广播给各个节点
2. 若一半以上节点持久化成功，则该请求对应的日志条目被标记为已提交
3. etcdserver 模块异步从raft 模块获取已提交的日志条目，并应用到状态机（BoltDB 等）

```
client     set hello world                更新成功     get hello
           \                               ^             | 
Leader      一半以上节点同意，在Leader应用     |             |
             \                                           |
Follower1     在Follower1应用                             |
                                                         |
Follower2       还未应用                         在Follower2查，因为Follower2未应用，所以查到旧数据
```

如上，使用串行查，那么可能返回的还是旧的数据

线性读是etcd 默认支持的模式，需要经过Raft 协议模块，反应的是**集群共识**，因此在延时和吞吐量上相比串行读略差，但是保证了数据一致性。比如可以在Follower 收到客户端读请求的时候转发到Leader

在etcd 3.1 时引入了ReadIndex 机制，在保证串行读的时候，也能读到最新的数据

```
client     set hello world                更新成功     get hello
           \                               ^             |                           ^
Leader      一半以上节点同意，在Leader应用     |             |  ReadIndex                |
             \                                          (1)  / \                    (5)
Follower1     在Follower1应用                             | (2)  (3)                  |
                                                         |/      \                   |
Follower2       还未应用                           先去查Leader的Committed  等待(4)   再返回
```

1. 当Follower2 收到一个线性读请求时，它会首先从Leader 获取集群最新的已经提交的日志索引（Committed Index），上图中的(2)
2. Leader 收到ReadIndex 请求时，为防止脑裂等异常场景，会向Follower 节点发送心跳确认，一半以上的节点确认Leader 身份后才能将已提交的索引（Committed Index）返回给Follower2，上图中的(3)
3. 节点会等待，直到状态机已应用索引（Applied Index）大于等于Leader 已提交索引，上图中的(4)
4. 然后调用MVCC 模块，根据key 查询value，然后响应读请求，如上图的(5)

#### MVCC

MVCC 模块时为了解决etcd v2 不支持保存key 的历史版本，不支持多key 事务等问题而引入的，它核心由内存树形索引模块treeIndex（基于B 树实现的）和嵌入式KV 持久化存储数据库BoltDB 组成（BoltDB 是基于B+ 树实现的KV 存储，支持事务）

每次修改操作，生成一个新的版本号（revision），以版本号为key，value 为用户key-value 等信息组成的结构体存储到BoltDB 中

读取的时候先从treeIndex 获取key 对应的revision，再以revision 为key 去查询BoltDB，获取value 信息

>实际查询的时候，并不是直接去BoltDB 中查询，为了性能优化，会先查询buffer。这部分的技术细节是怎么实现的？数据一致性是怎么保证的？

>在业务系统架构中，Redis 和MySQL 的数据一致性也是一个很常见的问题！



## 写请求流程

比如客户端发起请求

```shell
etcdctl set hello world --endpoints 192.168.65.210:2379, 192.168.65.211:2379, 192.168.65.212:2379
```

客户端发送请求到Leader 节点，通过Raft 协议将请求转发给其他Follower

#### Quota 模块

主要用于检查当前etcd db 大小加上本次请求的key-value 之和是否超过了配额

#### KVServer 模块

经过Quota 检查之后，就到了KVServer，具体就是执行put 方法

KVServer 模块主要功能为：

1. 打包提案，将put 写请求内容打包成一个提案（Porpose）消息，带有一个唯一的id，提交给Raft 模块，通过`propc` 投递
2. 请求限速、检查：在提交提案之前，还有限速、鉴权、和大包检查

>这里面站在比较宏观的层面看这个问题，如果想看源码层面应用层与Raft 算法层的交互逻辑，可以参见上一篇总结

#### WAL 模块

首先将put 请求封装成一个Raft 日志条目

```go
type Entry struct {
    Term  uint64
    Index uint64
    Type  EntryType     // EntryNormal or EntryConfChange
    Data  []byte
}
```

具体的持久化流程如下：

1. 首先将Raft 日志条目内容（含term、index、提案内容）序列化后保存到WAL 记录的Data 字段，然后计算Data 的CRC 值，设置EntryTye，以上信息就组成了一个完整的WAL 记录
2. 最后计算WAL 记录的长度，顺序先写入WAL 长度（Len Field），然后写入记录内容，调用fsync 持久化到磁盘，完成将日志条目保存到持久化存储中
3. 当一半以上节点持久化此日志条目后，Raft 模块就会通过channel 告知etcdserver 模块，put 提案已经被集群多数节点确认了，提案状态为已提交，你可以执行提案内容了
4. etcd 模块从channel 中取出提案内容，添加到FIFO 调度队列，然后通过Apply 模块按人队顺序，异步、以此执行提案内容

#### Apply 模块

Apply 模块主要用于执行处于已提交状态的提案，将其应用到状态机

Apply 模块会在应用提案之前，首先判断当前提案是否已经被应用过，如果应用过直接返回，若未应用同时无db 配额满警告，则进入MVCC 模块，开始与持久化存储模块打交道

>如果执行过程中crash，重启后如何找回异常提案，再次应用呢？主要依赖WAL 日志，因为提交给Apply 模块执行的提案已获得多数节点确认、持久化，etcd 重启时，会从WAL 中解析出Raft 日志条目内容，追加到Raft 日志的存储中，并重放已提交的日志提案给Apply 模块进行执行（应用）

etcd 通过引入一个consistent index 的这段，来存储系统当前已经执行（应用）过的日志条目索引，实现幂等性

>这些怎么与InnoDB 的redolog 做关联考虑？另外InnoDB 还有undolog，它不同于WAL，主要是用于日志的回滚使用！

#### MVCC 模块

MVCC 模块时为了解决etcd v2 不支持保存key 的历史版本，不支持多key 事务等问题而引入的，它核心由内存树形索引模块treeIndex（基于B 树实现的）和嵌入式KV 持久化存储数据库BoltDB 组成（BoltDB 是基于B+ 树实现的KV 存储，支持事务）

每次修改操作，生成一个新的版本号（revision），以版本号为key，value 为用户key-value 等信息组成的结构体存储到BoltDB 中

读取的时候先从treeIndex 获取key 对应的revision，再以revision 为key 去查询BoltDB，获取value 信息

>TreeIndex 是B Tree 实现；BoltDB是B+ Tree 实现，是后续很好的用来学习数据结构的切入点！
