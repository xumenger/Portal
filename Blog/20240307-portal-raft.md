>对于Raft 算法进行总结梳理

相对于Paxos，Raft 最大的特性就是易于理解。为了达到这个目标，Raft 主要做了两方面的事情：

1. 问题分解：把共识算法分为三个子问题：领导者选举（Leader Election）、日志复制（Log Replication）、安全性（Safety）
2. 状态简化：对算法做了一些限制，减少状态数量和可能产生的变动。一台服务器只在三种状态之间进行切换，并且服务器之间的通信仅通过两类RPC 来完成

这三个子问题大体上就可以作为Raft 算法中的三个类来实现


## 复制状态机

相同的初始状态 + 相同的输入 = 相同的结束状态

多个节点上，从相同的初始状态开始，执行相同的一串命令，产生相同的最终状态。在Raft 中，Leader 将客户端请求（Command）封装到一个个Log Entry 中，将这些Log Entries 复制到所有Follower 节点，然后大家按相同顺序应用Log Entries 中的Command。根据复制状态机的理论，大家的结束状态肯定是一致的

可以说，使用共识算法就是为了实现复制状态机。一个分布式场景下的各个节点间，就是通过共识算法来保证命令序列的一致，从而始终保持它们的状态一致，从而实现高可用（投票选主是一种特殊的命令）

扩展：复制状态机的功能可以更加强大。比如两个副本一个采用行存储的数据结构存储数据，另一个采用列存储，只要它们初始数据相同，并持续发给它们相同的命令，那么同一时刻从两个副本中读取到的结果也是一样的，这就是一种HATP 的实现方法（比如TiDB）


## 状态简化

在任何时刻，每一个服务器节点都处于Leader、Follower 或Candidate 三个状态之一。相比于Paxos，这一点就极大简化了算法的实现，因为Raft 只考虑状态的切换，而不用像Paxos 那样考虑状态之间的共存和互相影响

```
-->  starts up  -->  Follower  

Follower  -->  times out, starts election  -->  Candidate

Candidate  -->  times out, new election  -->  Candidate

Candidate  -->  receives votes from majority of servers  --> Leader

Candidate  --> discovers current leader or new term  --> Follower

Leader  --> discovers current leader or new term  --> Follower
```

Raft 把时间分割成任意长度的任期（Term），任期用连续的整数标记

每一段任期从一次选举开始。在某些情况下，一次选举无法选出Leader（比如两个节点收到相同的票数），在这种情况下，这一任期会以没有Leader 结束；一个新的任期（包含一次新的选举）会很快重新开始。Raft 保证在任意一个任期内，最多只有一个Leader

```
 term 1                         term 2                         term 3                term 4
|<------->|<---------------->| |<------->|<---------------->| |<----------------->| |<------->|<---------------->| 
 election   normal operation    election   normal operation     no emerging leader   election   normal operation
```

任期的机制就可以非常明确地标识集群的状态。通过任期的比较，可以帮助我们确认一台服务器历史的状态。比如可以通过查看一台服务器是否具有`term 2` 任期内的日志，来判断它在`term 2`时间段内是否出现过宕机

Raft 算法中服务器节点之间使用RPC 进行通信，并且Raft 中只有两种主要的RPC：

1. RequestVote RPC：投票请求，由Candidate 在选举期发起
2. AppendEntries RPC：追加条目，由Leader 发起，用来复制日志和提供一种心跳机制

服务器之间通信的时候会交换当前任期号，如果一个服务器上的当前任期号比其他的小，那么服务器会将自己的任期号更新为较大的那个值

如果一个Candidate 或Leader 发现自己的任期号过期了，它会立即回到Follower 状态

如果一个节点接收到一个包含过期任期号的请求，它会直接拒绝这个请求


## 领导者选举

Raft 内部有一种**心跳机制**，如果存在Leader，那么它就会周期性地向所有Follower 发送心跳，来维持自己的地位。如果Follower 一段时间没有收到心跳，那么它就会认为系统中没有可用的Leader 了，然后开始进行选举

开始一个选举过程后，Follower 会进行以下逻辑处理：

1. 先**增加自己的当前任期号**
2. 并**转换为Candidate 状态**
3. 然后**投票给自己**
4. 并且**并行地向集群中的其他服务器节点发送投票请求（RequestVote RPC）**

对于一个Candidate 来说，它的投票选举阶段最终有三种可能的结果

1. 它获得超过半数选票赢得了选举 -> 成为Leader 并开始发送心跳，告知其他节点集群中已经出现了一个新的Leader，并且结束投票选举阶段
2. 其他节点赢得了选举 -> 收到Leader 的心跳后，如果新Leader 的任期号不小于自己当前的任期号，那么就从Candidate 回到Follower 状态
3. 一段时间之后没有任何获胜者（比如多个Follower 同时成为Candidate，得票太过分散，没有任何一个Candidate 的票超过半数） -> 每个Candidate 都在一个自己的随机选举超时时间（论文建议为150-300ms）后增加任期号开始新一轮投票

随机选举超时时间，按照论文建议的150-300ms，对于现在的一些OLTP 系统还是比较长的，毕竟Raft 论文发表于2014 年，现在已经过去了10 年了，所以如果要在现在的系统应用Raft 算法的话，对于这个选举时间还需要根据自身的网络和服务器性能进行一些调整

>这里初步分析，存在这样的一种异常情况，比如某个Follower 和其他的节点网络分区了，收不到Leader 的心跳，它误认为是Leader 挂了，那么它就会一直增加term、发起选举，但是选举失败，最终导致term 一直增大，最后网络问题解除之后，term 很大，按照上面的规则说明，如果其他的节点收到这个Follower 的消息发现自己的term 都小于这个Follower 的term，那么都自动降为Follower，这样不是有问题的吗？Raft 的Pre Vote 是如何优化这个问题的？

相关的RPC 接口定义如下：

```proto
// 请求投票RPC Request
type RequestVoteRequest struct {
    term int          // 自己当前的任期号
    candidated int    // 自己的ID
    lastLogIndex int  // 自己最后一个日志号
    lastLogTerm int   // 自己最后一个日志的任期
}

// 请求投票RPC Response
type RequestVoteResponse struct {
    term int          // 自己当前任期号
    voteGranted bool  // 自己会不会投票给这个candidate
}
```

对于没有成为Candidate 的Follower 节点，对于同一个任期，会按照先来先得的原则投出自己的选票（每个Follower 只有一张选票）

>为什么RequestVote RPC 中要有Candidate 最后一个日志的信息呢？安全性子问题中会给出进一步的说明！

【简单总结】：Raft 集群启动后，所有的节点都是Follower，第一个认识到集群中没有Leader 的节点会把自己变成Candidate，它会给自己的任期号加一，并发起请求投票Request 给其他的Follower，通常来说，这个先发优势使得它大概率会成为Leader。在一个Leader 任期结束或失效后，也是同样会继续进入这样一个任期的循环

领导者选举的内容并不多，但是这是理解后续日志复制和安全性的关键！


## 日志复制

Leader 被选举出来后，开始为客户端提供服务，那么客户端怎么知道新的Leader 是哪个节点呢？

客户端随机向一个节点，或者就是老Leader 发送请求，这时有三种情况

1. 这个节点正好是Leader，那就不用再找了，直接执行指令即可
2. 这个节点为Follower，那么可以通过心跳得知Leader 的ID，然后可以告知Client 该找谁
3. 找到的这个节点正好宕机了，也就是说没有响应，那么Client 只能再去找另一个节点，重复此过程即可（所以Client 中需要保存服务端集群中每个节点的信息，这个是配置化的，还是怎么样，再集群节点动态变化的情况，Client 怎么得知？）

Leader 接收到客户端的指令后，会把指令作为一个新的条目（Entry）追加（Append）到日志中去

一条日志中需要具备三个信息：

1. 状态机指令，比如对某个值进行某个操作，比如`set x = 3`
2. Leader 的任期号，日志中包含任期号，对于监测多个日志副本之间的不一致情况，和判定节点状态，都有重要的作用，如下图可以清楚的看到哪个节点缺了哪个任期号
3. 日志号（日志索引），日志的唯一标号，可以用来区分日志的前后关系

因为日志号是单调递增的，例如Leader 宕机的情况，会造成日志号相同的日志内容却不同的情况，所以只有日志号和任期号两个因素才能唯一确定一个日志

生成日志之后，Leader 就会把日志放到AppendEntries RPC 中，并行地发送给Follower，让它们复制该条目。当该条目被超过半数的Follower 复制后，Leader 就可以在本地执行该指令（我们把本地执行指令，也就是Leader 应用日志到状态机这一步，称为**提交**）并把结果返回给客户端。这就是复制状态机的核心逻辑，在所有服务器都不出错，且网络正常的情况下，这个机制就可以保证所有节点具备完成且正确的日志

>并不是日志复制超过半数的节点后就百分百会提交的！因为Follower 复制完成后，到Follower 通知Leader，再到Leader 完成提交，是需要时间的，这个时间内如果Leader 宕机了，这条日志虽然复制到了超过半数的节点，但并没有能提交，这一点在安全性章节会是一个重要的伏笔

如下所示，集群中每个节点的每一条日志信息，可以看到有的节点差了几个日志，有的节点甚至漏掉了整个任期，但是在`logindex 从1到 7` 都满足至少有半数的节点（包括Leader）都复制到了日志，Leader 就可以提交了

```
1     2     3     4     5     6     7     8        log index【该行表示日志索引】

1     1     1     2     3     3     3     3        leader【该行的1、2、3表示term】
x<-3  y<-1  y<-9  x<-2  x<-0  y<-7  x<-5  x<-4     【该行表示日志对应的状态机指令】

1     1     1     2     3                          follower【该行的1、2、3表示term】
x<-3  y<-1  y<-9  x<-2  x<-0                       【该行表示日志对应的状态机指令】

1     1     1     2     3     3     3     3        follower【该行的1、2、3表示term】
x<-3  y<-1  y<-9  x<-2  x<-0  y<-7  x<-5  x<-4     【该行表示日志对应的状态机指令】

1     1                                            follower【该行的1、2、3表示term】
x<-3  y<-1                                         【该行表示日志对应的状态机指令】

1     1     1     2     3     3     3              follower【该行的1、2、3表示term】
x<-3  y<-1  y<-9  x<-2  x<-0  y<-7  x<-5           【该行表示日志对应的状态机指令】

|<---------committed entries---------->|
```

如上所示，可以看到有些Follower 的进度是落后很多的，那怎么让这些Follower 追上Leader，并保证所有节点的额日志都是完整且顺序一致的呢？比如Follower 缓慢、Follower 宕机、Leader 宕机

【1】如果有Follower 因为某些原因没有给Leader 响应，那么Leader 会不断地重发追加条目请求（AppendEntries RPC），哪怕Leader 已经回复了客户端

【2】如果Follower 崩溃后恢复，这时Raft 追加条目的**一致性检查**生效，保证Follower 能按顺序恢复崩溃后缺失的日志，和上面Follower 响应慢相比，这种情况下有很多不同：Follower 崩溃期间可能发生了很多事情，有可能经历了多次选举，Leader 已经换了好几个了，并且Follower 恢复后的状态也都是未知的，当前Leader 并不知道这个Follower 在宕机前日志复制到哪里了（所以需要一致性检查）

>Raft 的一致性检查：Leader 在每一个发往Follower 的追加条目RPC 中，会放入前一个日志条目的索引位置和任期号，如果Follower 在他的日志中找不到前一个日志，那么它就会拒绝此日志，Leader 收到Follower 的拒绝后，会发送前一个日志条目，从而逐渐向前定位到Follower 第一个缺失的日志

>为什么让Leader 一个个往回找呢？Follower 直接把自己最后一个日志的日志号发给Leader 不就行了吗？又或者往回找的时候多隔几个，然后再用二分查找法逐步定位第一个缺失的日志不是更高效吗？Raft 设计者也考虑到了这个问题，并且在论文中说出了这样的优化思路，但他们认为这种优化是没有必要的，因为失败不经常发生，并且也不太可能会有很多不一致的日志条目。当然这一点仁者见仁，智者见智，具体实现的时候可以根据需求调整，这些并不是Raft 的核心！

【3】如果Leader 崩溃，那么崩溃的Leader 可能已经复制了日志到部分Follower 但还没有提交，而被选出的新Leader 又可能不具备这些日志，这样有部分Follower 的日志和新Leader 的日志不同了（投票选举阶段是不需要考虑未提交的日志的）

Raft 在这种情况下，Leader 通过强制Follower 复制它的日志来解决不一致的问题，这意味着Follower 中跟新Leader 冲突的日志条目会被新的Leader 的日志条目覆盖（因为没有提交，所以不违背**外部一致性**，所谓外部一致性就是服务端与客户端之间的状态）

针对上面的情况，老Leader 恢复后变成了Follower，它会收到信Leader 的AppendEntries RPC，然后进行上面所讲的一致性检查的过程，在一致性检查逐步向前回溯的过程中，会发现自己最后几个日志和新Leader 的这部分日志出现了不同；一些复制了未提交日志的Follower，也可能会遇到这种情况。比如下面这张图

```
1  2  3  4  5  6  7  8  9 10 11 12    log index

1  1  1  4  4  5  5  6  6  6          leader for term

1  1  1  4  4  5  5  6  6             possible follower a
1  1  1  4                            possible follower b
1  1  1  4  4  5  5  6  6  6  6       possible follower c
1  1  1  4  4  5  5  6  6  6  7  7    possible follower d
1  1  1  4  4  4  4                   possible follower e
1  1  1  2  2  2  3  3  3  3  3       possible follower f
```

最上面就是当前的Leader，这个时候Follower 中的c 和d 竟然比这个Leader 还多出两个日志，为什么Leader 没有这些日志还能当选为Leader 呢？因为c 和d 多出的日志没有提交，也就不构成大多数，在这个7 节点的集群中，Leader 可以依靠a、b、e 和自己的选票当选为leader（f 可以投票）

这时候它们多出的日志是不是就会和Leader 产生冲突了？

还有最后一个节点f，它具有的2、3 任期的日志，别的节点都不具备，这大概率意味着它在那两个任期内担任Leader，但是它在2、3 任期内的日志都没有正常复制到大多数节点，也就没有提交。这时如果节点f 恢复了，那么它在2、3 任期的日志都和Leader 中的不同，就产生了冲突

Raft 在这种情况下，通过强制Follower 复制Leader 的日志来颗觉不一致的问题。这意味着Leader 通过**一致性检查**找到Follower 中最后一个和自己一致的日志之后，就会把这之后Follower 和自己冲突的所有日志全部覆盖掉

>为什么可以这么做！！因为没有提交，不存在**外部一致性**的问题！！

追加条目RPC 的接口定义如下：

```proto
// 追加日志RPC Request
type AppendEntriesRequest struct {
    term int         // 自己当前的任期号
    leaderId int     // leader（也就是自己的ID）
    prevLogIndex int // 前一个日志的日志号，用于一致性检查
    prevLogTerm int  // 前一个日志的任期号，用于一致性检查
    entries []byte   // 当前日志体
    leaderCommit int // Leader的已提交日志号
}

// 追加日志RPC Response
type AppendEntriesResponse struct {
    term int         // 自己当前的任期号
    success bool     // 如果Follower 包含前一个日志，则返回true，通过一致性检查后才会返回true
}
```

提交是一个非常重要的状态，对于Follower 而言，接收到Leader 的日志，并不能立即提交，因为这个时候还没有确认这个日志是否被复制到了大多数节点，只有Leader 确认了日志被复制到大多数节点后，Leader 才会提交这个日志，也就是应用到自己的状态机中，然后Leader 会在AppendEntries RPC 中把这个提交信息告知Follower，也就是`leaderCommit`，然后Follower 就可以把自己复制但未提交的日志设置为已提交状态，就可以应用到自己的状态机里了

对于那些苦苦追赶的Follower 来说，leaderCommit 大于自己最后一个日志，这时它的所有日志都是可以提交的！

如果leaderCommit > commitIndex，那么把commitIndex 设为min(leaderCommit, index of last new entry) 


## 安全性

领导者选举和日志复制两个子问题实际上已经涵盖了共识算法的全程，但这两点还不能保证每个状态机会按照相同的**顺序**执行相同的命令。所以Raft 通过几个补充规则完善整个算法，使算法可以在各类宕机问题下都不出错

```
顺序，无空洞
1  2  3  4  5  6  7  8  9  10 11 12

非顺序，有空洞
1  2  3  4  5  6  7     9  10 11 
```

>日志复制阶段对于日志顺序的保证能生效的前提是Leader 是正常的，如果Leader 出现宕机，它的后几个日志的状态就有可能出现不正常，这时新Leader 是否具备这些不正常的日志，以及怎么处理这些不正常的日志就是非常关键的。这也是Raft 为数不多需要特殊处理的边界情况

所以安全性这几个子问题主要就是定义几个规则来完善Raft 算法，使得Raft 在各种便捷情况下都不出错误

这些规则包括（不讨论安全性条件的证明过程）：

1. Leader 宕机处理：选举限制
2. Leader 宕机处理：新Leader 是否提交之前任期内的日志条目
3. Follower 和Candidate 宕机处理
4. 时间和可用性限制

**【1】 Leader 宕机处理：选举限制**

如果一个Follower 落后Leader 若干条日志（但**没有漏一整个周期**），那么下次选举中，按照领导者选举里的规则，它依旧有可能当选Leader。它在当选新Leader 后就永远也无法补上之前缺失的那部分日志，从而造成状态机之间的不一致

>比如下面这种情况，Old Leader 和Follower 1之间日志同步，到`index = 9` 的条目，但是Follower 1 只同步到`index = 6`

>显然对于index 为7、8、9 的日志，满足3 个节点里面有2 个节点同步完成的大多数要求，Leader 可以提交，并且返回给客户端

>但这时Leader 宕机了，7、8、9 同步给了Follower 1，但是没有告诉Follower 1 已提交；同时7、8、9 没有来得及同步给Follower 2

>但是按照选举的规则，有可能Follower 2 而不是Follower 1 当选为新的Leader，那么7、8、9 就缺失了，但是实际客户端认为已经提交了

```
1  2  3  4  5  6  7  8  9   log index

1  1  1  1  1  1  1  1  1   Old Leader

1  1  1  1  1  1  1  1  1   Follower 1
1  1  1  1  1  1            Follower 2
```

所以需要对领导者选举增加一个限制，保证被选出来的Leader 一定包含了之前各个任期的所有**被提交**的日志条目（被提交的概念很重要）

所以需要对领导者选举增加一个限制，保证被选出来的Leader 一定包含了之前各任期的所有被提交的日志条目

在RequestVote RPC 中增加两个参数：`lastLogIndex`、`lastLogTerm`

```
// 请求投票RPC Request
type RequestVoteRequest struct {
    term int          // 自己当前的任期号
    candidated int    // 自己的ID

    // 增加下面的这两个参数的作用就是解决上述情况所说的问题
    lastLogIndex int  // 自己最后一个日志号
    lastLogTerm int   // 自己最后一个日志的任期
}
```

RequestVote RPC 执行了这样的限制：RPC 包含了Candidate 的日志信息，如果投票者自己的日志比Candidate 的还**新**，它会拒绝掉该投票请求

Raft 通过比较两份日志中最后一条日志条目的索引值和任期号来定义谁的日志比较新

* 如果两份日志最后条目的`term`（任期号） 不同，那么`term` 大的日志更**新**
* 如果两份日志最后条目的`term` 相同，那么日志较长的那个更**新**

```
    1 2    1 2    1 2 3    1 2 3    index
S1 [1 2]   1 2   [1 2 4]   1 3
S2  1 2    1 2    1 2      1 3
S3  1      1      1 2      1 3
S4  1      1      1        1 3
S5  1     [1 3]   1 3     [1 3]
    [a]    [b]    [c]      [d]
```

a 中，S1 是Leader；到了b 中，S1 宕机了，S5 通过S3、S4 的投票赢的选举；但到了c 中S5 又宕机了，S1 重启并且选举成功，此时日志2 已经被复制到了大多数机器上，但还没有被提交（如果这里面提交了呢？）；到了d 中，S1 又宕机了S5 通过S2、S3、S4 的选票能够再次选举成功

在d 中，为什么S2、S3 会投票给S5 呢？因为他们的日志index 相同，但是S5 的term 更大，所以是符合选举规则的

在c 到d 的时候，2 被复制到大多数节点，但是S5 当选为Leader 之后2 就丢了，这是不是不安全？那么需要结合下一条安全性规则

**【2】 Leader 宕机处理：新Leader 是否提交之前任期内的日志条目**

一旦当前任期内某个日志条目已经被存储到过半的服务器节点上，Leader 就知道该日志条目可以被提交了。但是这个提交是**单点提交**，而非**集群提交**

>在讲日志复制的时候提到，Leader 收到超过半数节点的复制成功反馈之后，就可以应用日志到自己的状态机中了，这一步就是Leader 中的提交，但这个时候Follower 节点虽然复制到了日志，可还没有应用到自己的状态机上，也就是没有提交。所以对整个集群来说，提交这个状态是并没有构成大多数的

一个是Leader 自身执行提交，另外Leader 需要告诉其他复制了日志的Follower 进行提交。Follower 的提交触发条件为下一个AppendEntries RPC（心跳或新日志）。具体是靠AppendEntriesRequest 中的`leaderCommit` 参数实现的

```
// 追加日志RPC Request
type AppendEntriesRequest struct {
    term int         // 自己当前的任期号
    leaderId int     // leader（也就是自己的ID）
    prevLogIndex int // 前一个日志的日志号，用于一致性检查
    prevLogTerm int  // 前一个日志的任期号，用于一致性检查
    entries []byte   // 当前日志体
    leaderCommit int // Leader的已提交日志号
}
```

所以Leader 提交和Follower 提交之间必然会间隔一段时间。如果Leader 提交之后直接返回客户端，在通知Follower 提交之前，如果Leader 宕机了，是不是就会出现返回Client 成功但是事务提交状态却没有在集群中保留下来的情况呢？

>这个确实在Raft 的论文中没有讨论。一些人的观点是：Raft 是一种底层的共识算法，本身只是应用实现高可用的一种方法，而与客户端交互本来应该是属于应用端的事情，理论上不是Raft 应该担心的，这也是论文中不讨论这一点的原因。通常来讲，要解决这个问题，应用会设置一个`集群提交`的概念，只有集群中超过半数的节点都完成提交了，才认为集群提交完成了

Raft 的Leader 可以通过AppendEntriesResponse 的success 与否，判定这个Follower 是否完成提交，所以Leader 可以很容易的判断一个日志是否符合集群提交的条件。这个过程也有点类似分布式事务两阶段提交的过程

```
// 追加日志RPC Response
type AppendEntriesResponse struct {
    term int         // 自己当前的任期号
    success bool     // 如果Follower 包含前一个日志，则返回true，通过一致性检查后才会返回true
}
```

>etcd 是怎么做的？

如果某个Leader 在提交某个日志条目之前崩溃了，以后的Leader 会试图完成该日志条目的**复制**（而非提交）。通过选举限制规则，一般情况下新的Leader 是具备老的Leader 已经提交的日志的，但是这些日志在新的Leader 中可能还没有提交，这时新Leader 会尝试将这些日志复制给其他所有其他Follower，但它不会提交

什么时候才会提交呢？**等到新的Leader 在它的任期内新产生一个日志，在这个日志提交时，老Leader 任期内的日志也就可以提交了**（怎么理解？）

比如下面的场景（注意与上一个场景做好比对）在c 阶段，S1 变成了Leader，产生了新的日志`index = 4`，c 到d 阶段，把`index = 4` 复制到其他节点，并且提交的时候，老Leader 任期内的`index = 2`的日志也就可以提交了

```
    1 2    1 2    1 2 3    1 2 3    index
S1 [1 2]   1 2   [1 2 4]  [1 2 4]
S2  1 2    1 2    1 2      1 2 4
S3  1      1      1 2      1 2 4
S4  1      1      1        1 
S5  1     [1 3]   1 3      1 3
    [a]    [b]    [c]      [d]
```

>2024.03.17 这部分内容还是有点绕，没有完全理解！！！！！

**【3】 Follower 与Candidate 宕机处理**

Follower 与Candidate 宕机后的处理方式比Leader 宕机简单的多，并且两者的处理方式时相同的

如果Follower 与Candidate 宕机了，那么后续发送给它们的Request Vote 和Append Entries 都会失败。Raft 通过**无限的重试**来处理这种失败。如果宕机的机器重启了，那么这些RPC 就会成功地完成

如果一个服务器在完成了一个RPC，但还没有响应的时候崩溃了，那么它重启之后就会再次收到相同的请求（**Raft 的RPC 都是幂等的**）

**【4】 时间与可用性限制**

Raft 算法整体是不依赖客观时间的，也就是说，哪怕因为网络后其他因素，造成后发的RPC 先到，也不会影响Raft 的正确性

只要整个系统满足下面的时间要求，Raft 就可以选举并维持一个稳定的Leader：

```
广播时间（broadcast time） << 选举超时时间（election timeout） << 平均故障时间（MTBF）
```

广播时间（broadcast time） 如果大于选举超时时间（election timeout），那么每次选举都会超时，那么就永远选不出来Leader 了！

广播时间和平均故障时间是有系统决定的。但是选举超时时间是可配置的

Raft 的RPC 需要接收并将信息落盘，所以广播时间大约是0.5ms 到20ms，取决于存储的技术。因此，选举超时时间可能需要在10ms 到500ms 之间。大多数服务器平均故障时间间隔都在几个月，甚至更长



## 集群成员变更

对于一个分布式系统而言，可扩展性是非常重要的，这意味着共识算法需要具备在集群中不停服务增减机器的能力

本次的系统梳理中暂时不考虑这一点，包括Snapshot 本次也没有整理，这些并不是Raft 的核心部分，等到Portal 把Raft 的基础核心功能实现之后，再来考虑优化这些内容！
