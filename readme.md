## Portal说明

基于Raft 分布式共识算法设计

使用C、C++、Lua 实现的分布式管理框架


## 架构设计

>绘图工具：[https://www.processon.com/diagrams](https://www.processon.com/diagrams)

![](./Blog/image/01.png)

整个框架分为四层

1. 基于Raft 实现的管理调度集群
2. 工作机器上的管理进程
3. 工作机器上的工作进程
4. 工作进程内基于so/lua 实现的业务逻辑


## 编译部署

**编译Java proto**

```shell

```

**编译C++ proto**

```shell
cd ./src/proto/
protoc -I=./ --cpp\_out=../raft/ ./raftProto.proto
```


## 全量相关文档

* [20240218 - Portal 开发过程中问题列表](./Blog/20240218-questions-list.md)
* [20240219 - 常用的命令行](./Blog/20240219-useful-command.md)
* [20240225 - etcd 是怎么处理网络消息的？](./Blog/20240225-etcd-raft-protobuf.md)
* [20240226 - TCP 通信时的大小端问题](./Blog/20240226-cpp-java-endian.md)
* [20240307 - 对于Raft 算法进行总结梳理](./Blog/20240307-portal-raft.md)
* [20240309 - IO 多路复用Reactor 网络库封装：Redis](./Blog/20240309-io-eventloop-redis.md)
* [20240317 - etcd 源码解读 - 1 - raft 算法层](./Blog/20240317-etcd-source-raft.md)
* [20240318 - etcd 源码解读 - 2 - 应用层](./Blog/20240318-etcd-source-app.md)
* [20240321 - 研究luna 的源码，大概摸清楚架构逻辑](./Blog/20240321-luna-source.md)

**client**

>以此工程推动研究Java、JVM、Java并发编程、Spring、HTTP、Servlet、Tomcat、MySQL、InnoDB、PostgreSQL、DDD、TDD、

* [20240220 - 搭建一个简单的SpringBoot 项目](./Blog/20240220-transfer-springboot-start.md)
* [20240222 - Java 与C++ 之间给基于Protobuf 指定TCP 通信协议](./Blog/20240222-java-transfer-protobuf.md)

**portal**

>以此工程推动研究C++、etcd、Raft、TCP/IP、网络编程、select、poll、epoll、

* [20240221 - 搭建初版的Portal 进程](./Blog/20240221-portal-start.md)
* [20240224 - Portal 服务端支持与Transfer、Agent 通信](./Blog/20240224-cpp-portal-protobuf.md)
* [20240227 - Portal 基于epoll 实现IO 多路复用](./Blog/20240227-cpp-portal-epoll.md)
* [20240228 - Portal 实现简单版本的KV 存储，支持set/get 方法](./Blog/20240228-portal-kvstore.md)
* [20240306 - Portal 框架架构实现](./Blog/20240306-portal-architecture.md)
* [20240319 - Portal 架构设计](./Blog/20240319-portal-architecture.md)

**agent**

>以此工程推动研究C++、Linux、多进程、多线程、

* [20240223 - Agent 程序基于Protobuf 与Portal 服务端通信](./Blog/20240223-cpp-agent-protobuf.md)
* [20240229 - 实现Agent 支持接收Portal 指令并fork 进程](./Blog/20240229-agent-fork.md)

**worker**

>以此工程推动研究C++、Lua、so、Linux、namespace、cgroup、Docker、

* [20240305 - Worker 支持加载指定SO 启动线程（废弃）](./Blog/20240305-worker-so.md)

## 开发日志

>2022.02.19

1. 定义Raft 算法的关键数据结构
2. 定义Protobuf 接口

>2023.12.14

1. 项目的目录结构如何划分
2. 项目的模块如何划分？
3. 怎么能够方便调试？
4. 可以应用什么设计模式？
5. 怎么做单元测试？

